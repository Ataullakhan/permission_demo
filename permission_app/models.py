from django.db import models
from django.db.models import Q
from django.contrib.auth.models import Group, User,Permission


# from permission import add_permission_logic
from permission.logics import CollaboratorsPermissionLogic


# class Article(models.Model):
#     title = models.CharField('title', max_length=120)
#     body = models.TextField('body')
#     project = models.ForeignKey('permission.Project')
#
#     # this is just required for easy explanation
#     class Meta:
#         app_label='permission'
#
# class Project(models.Model):
#     title = models.CharField('title', max_length=120)
#     body = models.TextField('body')
#     collaborators = models.ManyToManyField(User)
#
#     # this is just required for easy explanation
#     class Meta:
#         app_label='permission'
#
# # apply AuthorPermissionLogic to Article
#
# add_permission_logic(Article, CollaboratorsPermissionLogic(
#     field_name='project__collaborators',
# ))
#
#


# g = Group.objects.get(name='My Group Name')
# users = User.objects.all()
# for u in users:
#     g.user_set.add(u)
#
# def users(request):
#     users = User.objects.filter(is_active=1).exclude(id=request.user.id)
#     groups = Group.objects.exclude(Q(name='customer') | Q(name='vendor') | Q(name='labour'))
#     permissions = Permission.objects.all()
#
#     return {
#         'all_users': users,
#         'all_groups' : groups,
#         'permissions' : permissions
#     }
# class Task(models.Model):
#
#     class Meta:
#         permissions = (
#             ("view_task", "Can see available tasks"),
#             ("change_task_status", "Can change the status of tasks"),
#             ("close_task", "Can remove a task by setting its status as closed"),
#         )
