# from .base import *
from permission_demo.settings.base import *

DEBUG = True

# INSTALLED_APPS += [
#     'debug_toolbar',
# ]

# MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'permission_demo',
        'USER': 'hello_django',
        'PASSWORD': 'hello_django',
        'HOST': 'localhost',
        'PORT': '',                      # Set to empty string for default.
    }
}


# DEBUG_TOOLBAR_CONFIG = {
#     'JQUERY_URL': '',
# }