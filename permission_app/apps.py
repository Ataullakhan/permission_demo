from django.apps import AppConfig


class PermissionAppConfig(AppConfig):
    name = 'permission_app'
