from permission.logics import AuthorPermissionLogic
from permission.logics import CollaboratorsPermissionLogic

PERMISSION_LOGICS = (
    ('permission_app.Article', AuthorPermissionLogic()),
    ('permission_app.Article', CollaboratorsPermissionLogic()),
)
